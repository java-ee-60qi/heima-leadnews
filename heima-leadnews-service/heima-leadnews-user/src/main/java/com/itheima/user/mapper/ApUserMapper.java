package com.itheima.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.usre.pojos.ApUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * com.itheima.user.mapper
 *
 * @author: ZhangQiang
 * @date: 2023/9/3 19:28
 * @version: 1.0
 */
@Mapper
public interface ApUserMapper extends BaseMapper<ApUser> {
}
