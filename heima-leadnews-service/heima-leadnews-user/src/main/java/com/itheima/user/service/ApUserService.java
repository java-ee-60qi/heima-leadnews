package com.itheima.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.usre.dtos.LoginDto;
import com.heima.model.usre.pojos.ApUser;

/**
 * com.itheima.user.service
 *
 * @author: ZhangQiang
 * @date: 2023/9/3 19:34
 * @version: 1.0
 */
public interface ApUserService extends IService<ApUser> {
    /**
     * 用户登录
     * @param loginDto
     * @return
     */
    ResponseResult login(LoginDto loginDto);
}
