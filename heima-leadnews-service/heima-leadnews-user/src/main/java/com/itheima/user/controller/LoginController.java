package com.itheima.user.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.usre.dtos.LoginDto;
import com.itheima.user.service.ApUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * com.itheima.user.controller
 *
 * @author: ZhangQiang
 * @date: 2023/9/3 16:38
 * @version: 1.0
 */
@RestController
@RequestMapping("/api/v1/login")
@Slf4j
public class LoginController {

    @Autowired
    private ApUserService apUserService;

    /**
     * app用户登录
     * @param loginDto
     * @return {@link ResponseResult}
     */

    @PostMapping("/login_auth")
    public ResponseResult loginAuth(@RequestBody LoginDto loginDto){
        log.info("app登录:{}",loginDto);
        return apUserService.login(loginDto);

    }
}
