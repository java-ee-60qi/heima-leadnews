package com.itheima.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.usre.dtos.LoginDto;
import com.heima.model.usre.pojos.ApUser;
import com.heima.utils.common.AppJwtUtil;
import com.itheima.user.mapper.ApUserMapper;
import com.itheima.user.service.ApUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * com.itheima.user.service.impl
 *  利用MyBatis-Plus
 * @author: ZhangQiang
 * @date: 2023/9/3 19:34
 * @version: 1.0
 */
@Service
@Transactional
@Slf4j
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper,ApUser> implements ApUserService{

    /**
     * 用户登录
     *
     * @param loginDto
     * @return
     */
    @Override
    public ResponseResult login(LoginDto loginDto) {
        //0, 业务层代码校验
        if (StringUtils.isEmpty(loginDto.getPhone()) && StringUtils.isEmpty(loginDto.getPassword())){
            //游客登录 只有token
            //3.2 密码正确: 登录成功 基于用户id生成token返回给前端 AppJwtUtil
            String token = AppJwtUtil.getToken(0L);
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("token",token);
            return ResponseResult.okResult(resultMap);

        }else if (!StringUtils.isEmpty(loginDto.getPhone()) && !StringUtils.isEmpty(loginDto.getPassword())){
            //正常登录
            //1, 根据用户手机号查找用户信息
            ApUser apUser = getOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone,loginDto.getPhone()));
            //2, 判断用户是否存在
            //2.1 不存在: 直接结束 返回用户不存在异常信息
            if (apUser == null){
                return ResponseResult.errorResult(AppHttpCodeEnum.USER_NOT_EXIST);
            }

            //2.2 存在: 拿用户输入的密码与数据库中的盐进行拼接,拼接以后进行MD5加密处理 得到密文
            String inputPwd = DigestUtils.md5DigestAsHex((loginDto.getPassword() + apUser.getSalt()).getBytes());

            //3, 将计算的密文与数据库中的密文进行比对
            //3.1 密码错误: 直接结束 返回用户或密码错误信息
            if (!inputPwd.equals(apUser.getPassword())){
                return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
            }

            //3.2 密码正确: 登录成功 基于用户id生成token返回给前端 AppJwtUtil
            String token = AppJwtUtil.getToken(Long.valueOf(apUser.getId()));

            //4, 登录成功 将用户信息也返回给前端
            Map<String, Object> userMap = new HashMap<>();
            userMap.put("id",apUser.getId());
            userMap.put("password",apUser.getPassword());
            userMap.put("phone",apUser.getPhone());
            //5, 封装一个大的Map,将token和user信息封装进去
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("user",userMap);
            resultMap.put("token",token);
            return ResponseResult.okResult(resultMap);
        }else {
            //错误登录(返回无效参数)
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
    }
}

