package com.heima.article.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.ApArticleService;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * com.heima.article.service.impl
 * ApArticle 文章业务层
 *
 * @author: ZhangQiang
 * @date: 2023/9/4 11:30
 * @version: 1.0
 */
@Service
public class ApArticleServiceImpl extends ServiceImpl<ApArticleMapper, ApArticle> implements ApArticleService {

    @Autowired
    private ApArticleMapper apArticleMapper;

    /**
     * app端文章加载首页
     *
     * @param homeDto
     * @return
     */
    @Override
    public ResponseResult load(ArticleHomeDto homeDto,int type) {
        //1, 参数校验
        if (homeDto == null) {
            //501 无效参数
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2, 数据处理(前端有传时间值,如果没有传则会执行此段逻辑)
        if (homeDto.getMaxBehotTime() == null){
            //时间原点(1970年)
            homeDto.setMaxBehotTime(new Date(0L));
        }
        if (homeDto.getMinBehotTime() == null){
            //当前系统时间
            homeDto.setMinBehotTime(new Date());
        }
        if (homeDto.getSize() == null || homeDto.getSize() <= 0){
            //防止分页条数过小, <=0 设置为10条
            homeDto.setSize(10);
        }else{
            //防止分页条数过大 限制每页条数最大 50条
            homeDto.setSize(Math.min(homeDto.getSize(),50));
        }

        //3, 调用Mapper层处理数据
        List<ApArticle> list = apArticleMapper.loadApArticle(homeDto,type);

        //4, 返回结果
        return ResponseResult.okResult(list);
    }
}
