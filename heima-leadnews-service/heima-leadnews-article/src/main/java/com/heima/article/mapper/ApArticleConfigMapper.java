package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Mapper;

/**
 * com.heima.article.mapper
 *
 * @author: ZhangQiang
 * @date: 2023/9/6 16:08
 * @version: 1.0
 */
@Mapper
public interface ApArticleConfigMapper extends BaseMapper<ApArticle> {
}
