package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ApArticleMapper extends BaseMapper<ApArticle> {

    /**
     * 首页加载    type : 1
     * 加载更多    type : 1
     * 加载更新    type : 2
     * @param homeDto
     * @return
     */
    List<ApArticle> loadApArticle(@Param("homeDto") ArticleHomeDto homeDto,
                                  @Param("type") int type);
}
