package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;

/**
 * com.heima.article.service
 *
 * @author: ZhangQiang
 * @date: 2023/9/4 11:30
 * @version: 1.0
 */
public interface ApArticleService extends IService<ApArticle> {
    /**
     * app端文章加载首页
     * @param homeDto
     * @return
     */
    ResponseResult load(ArticleHomeDto homeDto,int type);
}
