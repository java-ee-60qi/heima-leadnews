package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleContent;
import org.apache.ibatis.annotations.Mapper;

/**
 * com.heima.article.mapper
 *
 * @author: ZhangQiang
 * @date: 2023/9/6 16:07
 * @version: 1.0
 */
@Mapper
public interface ApArticleContentMapper extends BaseMapper<ApArticleContent> {
}
