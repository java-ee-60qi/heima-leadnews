package com.heima.model.usre.dtos;

import lombok.Data;

/**
 * com.heima.model.usre.dtos
 *
 * @author: ZhangQiang
 * @date: 2023/9/3 16:43
 * @version: 1.0
 */
@Data
public class LoginDto {
    /**
     * 手机号
     */
    private String phone;

    /**
     * 密码
     */
    private String password;
}
