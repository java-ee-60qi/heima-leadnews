package com.heima.model.article.dtos;

import lombok.Data;

import java.util.Date;

/**
 * com.heima.model.article.dtos
 *
 * @author: ZhangQiang
 * @date: 2023/9/4 11:26
 * @version: 1.0
 */
@Data
public class ArticleHomeDto {
    // 最大时间
    Date maxBehotTime;
    // 最小时间
    Date minBehotTime;
    // 分页size
    Integer size;
    // 频道ID
    String tag;
}
