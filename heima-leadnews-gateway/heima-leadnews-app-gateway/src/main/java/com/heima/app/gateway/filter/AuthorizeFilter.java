package com.heima.app.gateway.filter;


import com.heima.app.gateway.util.AppJwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.naming.ldap.StartTlsResponse;
import javax.security.auth.login.LoginContext;

/**
 * 基于全局过滤器实现统一登录验证
 * 2023.09.04
 */
@Slf4j
@Component
public class AuthorizeFilter implements GlobalFilter   {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("接收到了请求:{}", exchange.getRequest().getPath());

        //1, 拿到请求Request  响应Response对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        //2, 通过Request 拿到本次请求路径
        String path = request.getPath().toString();

        //3, 如果是登录相关的请求,直接放行
        if (path.contains("login")){
            //放行
            return chain.filter(exchange);
        }

        //4, 如果不是登录的请求,需要校验Token
        //4.1 从请求头中获取token
        String token = request.getHeaders().getFirst("Token");

        /**
         *  4.2 token有效 放行
         *       有效 a:不能为空
         *           b:不能伪造,不能过期
         */
        // 不能为空
        if (StringUtils.isEmpty(token)){
            // 返回401
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        // 不能伪造
        Claims claimsBody = AppJwtUtil.getClaimsBody(token);
        if (claimsBody == null){
            // 返回401
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        // 不能过期
        int res = AppJwtUtil.verifyToken(claimsBody);
        if (res == 1 || res == 2){
            // 返回401
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        //4.3 token有效 放行
        //放行
        return chain.filter(exchange);
    }
}
