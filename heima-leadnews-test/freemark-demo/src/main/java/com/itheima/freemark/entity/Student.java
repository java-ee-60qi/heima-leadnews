package com.itheima.freemark.entity;

import lombok.Data;

import java.util.Date;

/**
 * com.itheima.freemark.entity
 *
 * @author: ZhangQiang
 * @date: 2023/9/6 10:59
 * @version: 1.0
 */
@Data
public class Student {
        private String name;//姓名
        private int age;//年龄
        private Date birthday;//生日
        private Float money;//钱包
}
