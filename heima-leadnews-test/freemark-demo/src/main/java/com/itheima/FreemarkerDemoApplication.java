package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * com.itheima
 *
 * @author: ZhangQiang
 * @date: 2023/9/6 11:00
 * @version: 1.0
 */
@SpringBootApplication
public class FreemarkerDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(FreemarkerDemoApplication.class,args);
    }
}
